#include "ccv.h"
#include <sys/time.h>
#include <ctype.h>

int main(int argc, char** argv)
{
	assert(argc == 3);
	int i;
ccv_enable_default_cache();
	ccv_dense_matrix_t* image = 0;
	ccv_icf_classifier_cascade_t* cascade = ccv_icf_read_classifier_cascade(argv[2]);
	ccv_read(argv[1], &image, CCV_IO_ANY_FILE | CCV_IO_RGB_COLOR);
	assert(image != 0);
	
	ccv_array_t* seq = ccv_icf_detect_visualize(image, &cascade, ccv_icf_default_params);
	for (i = 0; i < seq->rnum; i++)
	{
	  ccv_comp_t* comp = (ccv_comp_t*)ccv_array_get(seq, i);
	  printf("%d %d %d %d %f\n", comp->rect.x, comp->rect.y, comp->rect.width, comp->rect.height, comp->classification.confidence);
	}
	ccv_array_free(seq);
	ccv_matrix_free(image);

	ccv_icf_classifier_cascade_free(cascade);
	ccv_disable_cache();
	return 0;
}
